# radicale_storage_mongo

MongoDB storage plugin for Radicale.


# How to use this plugin

To use this plugin, set `type = radicale_storage_mongo` in the `[storage]` section of
the Radicale configuration file.

An example (with extra configuration keys) is available in `radicale.cfg`.


# How to override specific functions

The plugin is built to be overriden for specific use cases. The main properties
that can be adapted are the `MONGO_TO_ITEM`, `ITEM_TO_MONGO` and
`Collection.user_id` objects.

`MONGO_TO_ITEM` is a dictionary mapping Mongo fields to iCal fields.
`ITEM_TO_MONGO` is the inverse mapping. The keys of these dictionaries are the
names of the properties, and the values are one between:

- `None` if the key has to be ignored,
- a string if the source property is mapped as-is to a destination property,
- a function if the mapping needs specific code.

`Collection.user_id` is a property function returning a user ID (used in Mongo)
corresponding to its address (used for authentication and in the calendar URL).

To override these properties, create a new `my_radicale_storage_mongo.py` file, and
adapt the code to your needs. Here is an example:

```
import radicale_storage_mongo


def item_attendees_to_mongo(key, fields, vobject_event, mongo_event):
    """Store iCal attendee fields in the Mongo event."""
    mongo_event['my_attendees'] = ', '.join(field.value for field in fields)


# Store the 'summary' iCal field in the 'my_title' Mongo field
radicale_storage_mongo.MONGO_TO_ITEM['my_title'] = 'summary'
# Store the 'attendee' Mongo field using a specific function
radicale_storage_mongo.ITEM_TO_MONGO['attendee'] = item_attendees_to_mongo


# Override user ID used in Mongo
class Collection(radicale_storage_mongo.Collection):
    @property
    def user_id(self):
        """Use the first part of the user address as user ID."""
        return self._user_address.split('@')[0]


# Use new collection in storage
class Storage(radicale_storage_mongo.Storage):
    Collection = Collection


# Expose radicale_storage_mongo configuration needed for all Radicale plugins
PLUGIN_CONFIG_SCHEMA = radicale_storage_mongo.PLUGIN_CONFIG_SCHEMA

```

It is then possible to use this custom plugin using `type = my_radicale_storage_mongo`
in the `[storage]` section of the Radicale configuration file.


# How to build source and wheel packages

For example in Linux:

```
python3 -m venv /tmp/venv
/tmp/venv/bin/pip install flit
/tmp/venv/bin/flit install
/tmp/venv/bin/flit build
```

The packages are generated in the `dist` folder.
